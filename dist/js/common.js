'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

// placeholder
//-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

    function firstSymbols() {
        $('.js-name').each(function () {
            var firstSym = $(this).text().slice(0,1),
                secondSym = $(this).text().split(' ')[1].slice(0,1),
                full = firstSym + secondSym;
            $(this).parents('.elements__unit').find('.elements__str').text(full);
        });
    }

    function select(){
        $('select').select2({
            minimumResultsForSearch: -1
        });
    }

    function topSticky(){
        $("#sticky").sticky({topSpacing:0});
    }

    $('.content__title').clone().prependTo('.hidden');
    var titleWidth = $('.hidden .content__title').outerWidth();

    function scrollWindow(){
        var scroll = $(window).scrollTop();

        if (scroll > 30) {
            $('.content').addClass('active');
            $('.content__filter').css('transform','translate('+titleWidth+'px,-27px)');
        } else {
            $('.content').removeClass('active');
            $('.content__filter').css('transform','translate(0,0)');
        }
    }

    function filterBtns(){
        var btn = $('.js-filter-btns .btn'),
            options = $('#options');
        btn.on('click',function (e) {
            e.preventDefault();
            options.slideUp(
                function(){
                    $("#sticky").sticky('update');
                }
            );
            if ($(this).hasClass('active')) {
                btn.removeClass('active');
            } else {
                btn.removeClass('active');
                $(this).addClass('active');
                if ($(this).hasClass('filter')) {
                    options.slideDown();
                }
            }
        });
    }

    function removeHidden(){
        $('.hidden').remove();
    }

    firstSymbols();

    $(document).ready(function () {
        select();
        topSticky();
        filterBtns();
        removeHidden();
    });

    $(window).scroll(function () {
        scrollWindow();
    });
});
